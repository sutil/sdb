package com.br.sutildb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

class UpdaterTable {
	
	private static final String TAG = "UpdaterTable";
	private static final String ALTER_TABLE = " ALTER TABLE ";
	private static final String ADD_COLUMN = " ADD COLUMN ";
	
	
	public static void alterTables(SQLiteDatabase db, Context ctx, ArrayList<Class<?>> tables) {
      
        for (Class<?> table : tables) {
            try {
                String tableName = Util.getTableName(table);
                Cursor c = db.query(false, tableName, null, null, null, null, null, null, null);
                
                //all old Columns Names
                List<String> oldColumns = Arrays.asList(c.getColumnNames());
                
                //all new Columns
                List<ColumnAttribute> newColumns = Util.getTableColumn(((SutilApplication) ctx.getApplicationContext()), table);
                StringBuffer sb1 = new StringBuffer();
                for (ColumnAttribute col : newColumns) {
                    if (!oldColumns.contains(col.getName())) {
                        sb1.append(col.getName());
                        sb1.append(" ");
                        sb1.append(col.getType());
                        if (col.getLength() > 0) {
                            sb1.append("(");
                            sb1.append(col.getLength());
                            sb1.append(")");
                        }
                        if (col.getDefault_value() != null) {
                            sb1.append(" default " + col.getDefault_value());
                        }
                        String sqlAlter = String.format("%s%s%s%s;", ALTER_TABLE, tableName, ADD_COLUMN, sb1.toString());
                        Log.i(TAG, sqlAlter);
                        db.execSQL(sqlAlter);
                        sb1 = new StringBuffer();
                    }
                }

            } catch (SQLiteException e) {
            	Log.i(TAG, "Não há alterações para a tabela, ela será criada");
            	FactoryTables.create(table, ctx, db);
            }
        }
    }

}
