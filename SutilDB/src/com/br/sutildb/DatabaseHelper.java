package com.br.sutildb;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.br.sutildb.annotation.Table;

import dalvik.system.DexFile;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	private static final String  TAG = "DatabaseHelper";
	
    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context, MetaData.getDBName(context), null, MetaData.getDBVersion(context));
        this.mContext = context;
        scanTables(getWritableDatabase());
        
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        ArrayList<Class<?>> entities = getEntityClasses(mContext);
        Log.i(TAG, "Creating " + entities.size() + " tables");
        for (Class<?> entity : entities) {
            FactoryTables.create(entity, mContext, db);
        }
        for (Class<?> entity : entities) {
            FactoryRelationchipOneToMany.createOneToMany(entity, db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");

        ArrayList<Class<?>> tables = getEntityClasses(this.mContext);
        for (Class<?> table : tables) {
            db.execSQL("DROP TABLE IF EXISTS "
                    + Util.getTableName(table));
        }
        onCreate(db);
    }

    private void scanTables(SQLiteDatabase db) {
        ArrayList<Class<?>> tables = getEntityClasses(mContext);
        UpdaterTable.alterTables(db, mContext, tables);
       
    }

    private static ArrayList<Class<?>> getEntityClasses(Context context) {
        ArrayList<Class<?>> entityClasses = new ArrayList<Class<?>>();
        try {
            String packageName = context.getPackageName();
            String path = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir;
            DexFile dexfile = new DexFile(path);
            Enumeration<String> entries = dexfile.entries();
            while (entries.hasMoreElements()) {
                String name = (String) entries.nextElement();
                if (name.contains(packageName)) {
                    Log.i(TAG, "Found class: " + name);
                    Class<?> discoveredClass = null;
                    try {
                        discoveredClass = Class.forName(name, true, context.getClass().getClassLoader());
                    } catch (ClassNotFoundException e) {
                        Log.e(TAG, e.getMessage());
                    }
                    if ((discoveredClass == null) || discoveredClass.getAnnotation(Table.class) == null) {
                        continue;
                    }
                    Log.i(TAG, "Create table for class "+ discoveredClass.getName());
                    Log.i(TAG, "Create table with name "+ discoveredClass.getAnnotation(Table.class).name());
                    entityClasses.add((Class<?>) discoveredClass);
                }
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        return entityClasses;
    }

    

}
