package com.br.sutildb;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.br.sutildb.annotation.Column;
import com.br.sutildb.annotation.ManyToOne;
import com.br.sutildb.annotation.Table;
import com.br.sutildb.annotation.TypeColumn;

public class Util {

	public static final String TAG = "Sutil Util";

	public final static List<ColumnAttribute> getTableColumn(SutilApplication application, Class<?> object) {
		List<ColumnAttribute> atas;
		
		if (application != null) {
			atas = application.getTableAttributes(object);
			if (atas != null) {
				return atas;
			}
		}
		atas = new ArrayList<ColumnAttribute>();
		Field[] fields = object.getDeclaredFields();
		if (fields.length == 0) {
			fields = object.getFields();
		}
		if (fields.length == 0) {
			fields = object.getSuperclass().getDeclaredFields();
		}
		Column columnAnnotation;
		for (Field field : fields) {
			columnAnnotation = field.getAnnotation(Column.class);
			if (columnAnnotation != null) {
				
				ColumnAttribute columnAttr = new ColumnAttribute(getName(columnAnnotation, field), getType(field, columnAnnotation), getLength(field, columnAnnotation), field);
				field.setAccessible(true);
				
				if (!columnAnnotation.default_value().equals("null")) {
					columnAttr.setDefault_value(columnAnnotation.default_value());
				}
				if ((columnAttr.getName().equals("_id") || columnAttr.getName().equals("id")) && columnAttr.getLength() > 0 && columnAnnotation.autoincrement() != false) {
					columnAttr.setAutoincrement(true);
					columnAttr.setPrimary(true);
				} else {
					columnAttr.setAutoincrement(columnAnnotation.autoincrement());
					columnAttr.setPrimary(columnAnnotation.primary());
				}
				atas.add(columnAttr);
			}
		}
		// cache
		if (application != null && atas != null) {
			application.addTableAttributes(object, atas);
		}
		return atas;
	}
	
	private static String getName(Column c, Field f) {
		if("".equals(c.name())){
			return f.getName();
		}
		return c.name();
	}
	
	private static String getType(Field field, Column c) {
		if (isSQLFloat(field.getType()) > 0) {
			return "REAL";
		} else if (isSQLInteger(field.getType()) > 0) {
			return "INTEGER";
		} else if (isSQLString(field.getType()) > 0) {
			return "TEXT";
		} else if (isSQLDate(field.getType())) {
			return "NUMERIC";
		}
		return c.type().getType();
	}
	
	

	private static int getLength(Field field, Column c){
		int len = 0;
		if((len = isSQLInteger(field.getType())) > 0) {
			return len;
		}
		else if(isSQLDate(field.getType())) {
			return 0;
		}
		else {
			if(c.type().equals(TypeColumn.INTEGER)) {
				return 8;
			}
			else {
				return 0;
			}
		}
	}
	
	public final static List<ForeignAttribute> getForeign(SutilApplication application, Class<?> object) throws Exception{
		List<ForeignAttribute> atts = new ArrayList<ForeignAttribute>();
		if (application != null) {
			atts = application.getForeigsAttibutes(object);
			if (atts != null) {
				return atts;
			}
		}
		atts = new ArrayList<ForeignAttribute>();
		Field[] fields = object.getDeclaredFields();
		if (fields.length == 0) {
			fields = object.getFields();
		}
		if (fields.length == 0) {
			fields = object.getSuperclass().getDeclaredFields();
		}
		ManyToOne m;
		for (Field field : fields) {
			m = field.getAnnotation(ManyToOne.class);
			if(m != null) {
				field.setAccessible(true);
				ForeignAttribute foreignAttr = ForeignAttribute.newInstance(m.name(), m.referencedTable(), m.referencedColumn(), field);
				atts.add(foreignAttr);
			}
		}
		if(application != null && atts != null) {
			application.addForeigsAttibutes(object, atts);
		}
		return atts;
	}

	/**
	 * @param object
	 *            the the class's name
	 * @return String 
	 * 			  name of table
	 */
	public final static String getTableName(Class<?> object) {
		Table t = object.getAnnotation(Table.class);
		if (t == null || "".equals(t.name())) {
			return object.getSimpleName();
		} else {
			return t.name();
		}
	}

	public final static boolean hasColumn(String column, String[] columns) {
		for (String c : columns) {
			if (c.equals(column)) {
				return true;
			}
		}
		return false;
	}

	public final static void loadModel(Class<?> object, Cursor cursor, Object model) {
		Field[] fields = object.getDeclaredFields();
		Column tmp_c;
		for (Field field : fields) {
			tmp_c = field.getAnnotation(Column.class);
			String fieldName = "";
			if (tmp_c != null) {
				fieldName = tmp_c.name();
				if (fieldName.equals("")) {
					fieldName = field.getName();
				}
			}
			Class<?> fieldType = field.getType();
			int columnIndex = cursor.getColumnIndex(fieldName);
			if (columnIndex < 0) {
				continue;
			}
			field.setAccessible(true);
			try {
				if ((fieldType.equals(Boolean.class)) || (fieldType.equals(Boolean.TYPE))) {
					field.set(model, Boolean.valueOf(cursor.getInt(columnIndex) != 0));
				} else if (fieldType.equals(Character.TYPE)) {
					field.set(model, Character.valueOf(cursor.getString(columnIndex).charAt(0)));
				} else if (fieldType.equals(java.util.Date.class) && cursor.getLong(columnIndex) > 0) {
					field.set(model, new java.util.Date(cursor.getLong(columnIndex)));
				} else if (fieldType.equals(java.sql.Date.class)) {
					field.set(model, new java.sql.Date(cursor.getLong(columnIndex)));
				} else if ((fieldType.equals(Double.class)) || (fieldType.equals(Double.TYPE))) {
					field.set(model, Double.valueOf(cursor.getDouble(columnIndex)));
				} else if ((fieldType.equals(Float.class)) || (fieldType.equals(Float.TYPE))) {
					field.set(model, Float.valueOf(cursor.getFloat(columnIndex)));
				} else if ((fieldType.equals(Integer.class)) || (fieldType.equals(Integer.TYPE))) {
					field.set(model, Integer.valueOf(cursor.getInt(columnIndex)));
				} else if ((fieldType.equals(Long.class)) || (fieldType.equals(Long.TYPE))) {
					field.set(model, Long.valueOf(cursor.getLong(columnIndex)));
				} else if (fieldType.equals(String.class)) {
					field.set(model, cursor.getString(columnIndex));
				} else if (fieldType.equals(BigDecimal.class)) {
					field.set(model, new BigDecimal(cursor.getDouble(columnIndex)));
				}
			} catch (IllegalArgumentException e) {
				Log.e(TAG, e.getMessage());
			} catch (IllegalAccessException e) {
				Log.e(TAG, e.getMessage());
			} catch (SecurityException e) {
				Log.e(TAG, e.getMessage());
			}
		}
	}

	public static final List<Object> processCursor(Context context, Class<?> object, Cursor cursor) {
		List<Object> entities = new ArrayList<Object>();
		try {
			if (cursor.moveToFirst()) {
				do {
					Object entity = object.newInstance();
					Util.loadModel(object, cursor, entity);
					entities.add(entity);
				} while (cursor.moveToNext());
			}
		} catch (InstantiationException e) {
			Log.e(TAG, e.getMessage());
		} catch (IllegalAccessException e) {
			Log.e(TAG, e.getMessage());
		} catch (SecurityException e) {
			Log.e(TAG, e.getMessage());
		}
		return entities;
	}

	public static final void setAutoId(SutilApplication application, Object obj, long id) {
		List<ColumnAttribute> columns = getTableColumn(application, obj.getClass());
		for (ColumnAttribute col : columns) {
			if (col.isAutoincrement()) {
				try {
					Class<?> fieldType = col.getField().getType();
					if ((fieldType.equals(Integer.class)) || (fieldType.equals(Integer.TYPE))) {
						col.getField().setInt(obj, (int) id);
					} else if ((fieldType.equals(Long.class)) || (fieldType.equals(Long.TYPE))) {
						col.getField().setLong(obj, id);
					}
				} catch (IllegalArgumentException e) {
					Log.e(TAG, e.getMessage());
				} catch (IllegalAccessException e) {
					Log.e(TAG, e.getMessage());
				}
				return;
			}
		}
	}

	private static int isSQLFloat(Class<?> object) {
		if ((object.equals(Double.class)) || (object.equals(Double.TYPE)) || 
				(object.equals(Float.class)) || (object.equals(Float.TYPE)) ||
				(object.equals(BigDecimal.class))) {
			return 1;
		}
		return 0;
	}

	private static int isSQLInteger(Class<?> object) {
		if (object.equals(Boolean.class) || object.equals(Boolean.TYPE)) {
			return 1;
		}
		if ((object.equals(Integer.class)) || (object.equals(Integer.TYPE))) {
			return 4;
		}
		if ((object.equals(Long.class)) || (object.equals(Long.TYPE))) {
			return 8;
		}
		return 0;
	}

	private static int isSQLString(Class<?> object) {
		if ((object.equals(String.class)) || (object.equals(Character.TYPE))) {
			return 1;
		}
		return 0;
	}
	
	private static boolean isSQLDate(Class<?> type) {
		return type.equals(java.util.Date.class) || type.equals(java.sql.Date.class);
	}
	
	

	public static String dumpToString(Object object) {
		Field[] fields = object.getClass().getDeclaredFields();
		StringBuilder sb = new StringBuilder();
		sb.append(object.getClass().getSimpleName()).append('{');

		boolean firstRound = true;

		for (Field field : fields) {
			if (!firstRound) {
				sb.append(", ");
			}
			firstRound = false;
			field.setAccessible(true);
			try {
				final Object fieldObj = field.get(object);
				final String value;
				if (null == fieldObj) {
					value = "null";
				} else {
					value = fieldObj.toString();
				}
				sb.append(field.getName()).append('=').append('\'').append(value).append('\'');
			} catch (IllegalAccessException ignore) {
				// this should never happen
			}
		}

		sb.append('}');
		return sb.toString();
	}
}
