package com.br.sutildb;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.br.sutildb.annotation.Column;
import com.br.sutildb.annotation.OneToMany;

class FactoryRelationchipOneToMany {
	
	private static final String TAG = "FactoryRelationchipOneToMany";
	
	
	public static void createOneToMany(Class<?> entity, SQLiteDatabase db) {
		String otherTable = "";
		String thisId = "";
		String thisTable = Util.getTableName(entity);
		Field[] fields = getFields(entity);
		OneToMany oneToMany;
		for (Field field : fields) {
			oneToMany = field.getAnnotation(OneToMany.class);
			if(oneToMany != null) {
				Class<?> typeList = getTypeList(field);
				otherTable = Util.getTableName(typeList);
				thisId = getColumnPrimaryKey(entity);
				createRelationship(thisTable, otherTable, thisId, db);
			}
		}
		
	}
	
	private static <T> Class<?> getTypeList(Field field) {
		ParameterizedType pType = (ParameterizedType) field.getGenericType();
        Class<?> clazz = (Class<?>) pType.getActualTypeArguments()[0];
        System.out.println(clazz); 
        return clazz;
        
	}
	
	private static void createRelationship(String thisTable, String otherTable, String thisId, SQLiteDatabase db) {
		Log.i(TAG, "Creating relationship "+otherTable+" "+thisTable);
		 String sql = getSqlAlterTable(thisTable, otherTable);
		 Log.i(TAG, sql);
		 db.execSQL(sql);
	}
	
	
	private static Field[] getFields(Class<?> entity) {
		Field[] fields = entity.getDeclaredFields();
		if (fields.length == 0) {
			fields = entity.getFields();
		}
		if (fields.length == 0) {
			fields = entity.getSuperclass().getDeclaredFields();
		}
		return fields;
	}
	
	
	private static String getColumnPrimaryKey(Class<?> entity) {
		Field[] fields = entity.getFields();
		String nameId = null;
		for(Field f : fields) {
			Column column = f.getAnnotation(Column.class);
			if(!column.autoincrement()) {
				continue;
			}
			nameId = f.getName();
		}
		if(nameId != null) {
			return nameId;
		}
		for(Field f : fields) {
			Column column = f.getAnnotation(Column.class);
			if(column.primary()) {
				nameId = f.getName();
			}
		}
		return nameId != null ? nameId : "";
	}
	
	
	
	private static String getSqlAlterTable(String thisTable, String otherTable) {
		String sqlAlterTable = String.format("ALTER TABLE %s ADD COLUMN %s INTEGER(8);",
				otherTable, thisTable);
		return sqlAlterTable;
	}
	
	
	
		
}
