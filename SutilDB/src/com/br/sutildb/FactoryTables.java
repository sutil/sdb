package com.br.sutildb;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

class FactoryTables {

	private static final String TAG = "FactoryTables";
	private static final String CREATE_TABLE = " CREATE TABLE "; 
	private static final String PRIMARY_KEY_AUTOINCREMENT= " PRIMARY KEY AUTOINCREMENT ";
	private static final String DEFAULT = " default ";
	private static final String PRIMARY_KEY = " PRIMARY KEY ";
	private static final String FOREIGN_KEY = " FOREIGN KEY ";
	private static final String REFERENCES = " REFERENCES  ";

	public static void create(Class<?> entity, Context ctx, SQLiteDatabase db) {
		StringBuffer sb = new StringBuffer();
		List<ColumnAttribute> columns = Util.getTableColumn((SutilApplication) ctx.getApplicationContext(), entity);
		if (columns.size() > 0) {
			sb.append(getColumns(columns));
			String strForeign = "";
			try {
				strForeign = getForeignKeys((SutilApplication) ctx.getApplicationContext(), entity);
			} catch (Exception e) {
				Log.e(TAG, "Error create foreign key to table " + Util.getTableName(entity));
				e.printStackTrace();
			}
			if (!"".equals(strForeign)) {
				sb.append(strForeign);
			}
			String sql = CREATE_TABLE + Util.getTableName(entity) + " (" + sb.toString().substring(0, sb.length() - 2) + " )";
			Log.i(TAG, sql);
			db.execSQL(sql);
		}
	}
	
	private static String getColumns(List<ColumnAttribute> columns) {
    	StringBuilder sb = new StringBuilder();
    	for(ColumnAttribute column : columns) {
    		
    		sb.append(column.getName());
            sb.append(" ");
            sb.append(column.getType());
            if (column.isAutoincrement()) {
                sb.append(PRIMARY_KEY_AUTOINCREMENT);
            }
            else {
                if (column.getLength() > 0) {
                    sb.append("(");
                    sb.append(column.getLength());
                    sb.append(")");
                }
                if (column.getDefault_value() != null) {
                    sb.append(DEFAULT + column.getDefault_value());
                }
            }
            sb.append(" , ");
    	}
    	sb.append(getPrimaryKeys(columns));
    	return sb.toString();
    }
	
	private static String getPrimaryKeys(List<ColumnAttribute> columns) {
    	String pms = "";
    	boolean haveAutoincrement = false;
    	for(ColumnAttribute column : columns) {
    		if(column.isPrimary()) {
    			pms = String.format("%s%s,", pms, column.getName());
    		}
    		if(column.isAutoincrement()) {
    			haveAutoincrement = true;
    		}
    	}
    	if(!"".equals(pms) && !haveAutoincrement) {
    		return  PRIMARY_KEY+"(" + pms.substring(0, pms.length() - 1) + ") , ";
    	}
    	return "";
    }
	
	private static String getForeignKeys(SutilApplication application, Class<?> table) throws Exception {
    	List<ForeignAttribute> foreign = Util.getForeign(application, table);
    	StringBuilder sb = new StringBuilder();
    	for(ForeignAttribute f : foreign) {
    		sb.append(FOREIGN_KEY);
    		sb.append("(");
    		sb.append(f.getColumnName());
    		sb.append(") ");
    		sb.append(REFERENCES);
    		sb.append(f.getReferencedTable());
    		sb.append(" (");
    		sb.append(f.getColumnReferenced());
    		sb.append(" ) , ");
    	}
    	return sb.toString();
    }

}
