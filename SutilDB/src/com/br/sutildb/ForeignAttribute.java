package com.br.sutildb;

import java.lang.reflect.Field;

public class ForeignAttribute {
	private String columnName;
	private String referencedTable;
	private String columnReferenced;
	private Field field;
	
	private ForeignAttribute(String columnName, String referencedTable, String columnReferenced, Field field) {
		this.columnName = columnName;
		this.referencedTable = referencedTable;
		this.columnReferenced = columnReferenced;
		this.field = field;
	}
	
	public static ForeignAttribute newInstance (String columnName, String referencedTable, String columnReferenced, Field field) throws Exception {
		if(field != null && 
				columnName != null && !columnName.equals("") &&
				referencedTable != null && !referencedTable.equals("") &&
				columnReferenced != null && !columnReferenced.equals("")) {
			return new ForeignAttribute(columnName, referencedTable, columnReferenced, field);
		}
		throw new Exception("!!Attribute Foreign fail!!");
	}
	
	
	public String getColumnName() {
		return columnName;
	}
	
	public String getColumnReferenced() {
		return columnReferenced;
	}
	
	public Field getField() {
		return field;
	}
	
	public String getReferencedTable() {
		return referencedTable;
	}
	
}
