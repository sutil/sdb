package com.br.sutildb.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public abstract @interface Column
{
  public abstract String name() default "";
  public abstract boolean primary() default false;
  public abstract boolean autoincrement() default false;
  public abstract boolean notNull() default false;
  public abstract String default_value() default "null";
  public abstract TypeColumn type() default TypeColumn.INTEGER;
}