package com.br.sutildb.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public abstract @interface ManyToOne {
	public abstract String name();
	public abstract String referencedTable();
	public abstract String referencedColumn();
}
