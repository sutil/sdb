package com.br.sutildb.annotation;

public enum TypeColumn {
	
	TEXT("TEXT"), INTEGER("INTEGER"), DATE("NUMERIC");
	
	
	private TypeColumn(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	
	private String type;
	
}
