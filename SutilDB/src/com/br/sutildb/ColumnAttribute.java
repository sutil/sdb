package com.br.sutildb;

import java.lang.reflect.Field;

public class ColumnAttribute {
	
	private String name;
	private String type;
	private int length;
	private Field field;
	private String default_value = null;
	private boolean primary = false;
	private boolean foreign = false;
	private boolean autoincrement = false;
	private boolean notNull = false;
	
	
	public ColumnAttribute(String name, String type, int length, Field field) {
		this.name = name;
		this.type = type;
		this.length = length;
		this.field = field;
	}
	
	public String getName() {
		return name;
	}


	public String getType() {
		return type;
	}


	public int getLength() {
		return length;
	}


	public Field getField() {
		return field;
	}

	public String getDefault_value() {
		return default_value;
	}

	public void setDefault_value(String default_value) {
		this.default_value = default_value;
	}

	public boolean isPrimary() {
		return primary;
	}

	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	public boolean isForeign() {
		return foreign;
	}

	public void setForeign(boolean foreign) {
		this.foreign = foreign;
	}

	public boolean isAutoincrement() {
		return autoincrement;
	}

	public void setAutoincrement(boolean autoincrement) {
		this.autoincrement = autoincrement;
	}

	public boolean isNotNull() {
		return notNull;
	}

	public void setNotNull(boolean notNull) {
		this.notNull = notNull;
	}
	
	
	
	
}
